# freespace.py
# V32 10/20/18 - Liam Kennedy
# This will ensure there is always space available on the microSD card
# Designed to be run automatically every day at 11:30pm (before the auto update check)
# and also at startup.  
import time, subprocess, os

try: 
   import logging
   logger = logging.getLogger('freespace')
   hdlr = logging.FileHandler('/home/pi/ExoLab/freespace.log')
   formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
   hdlr.setFormatter(formatter)
   logger.addHandler(hdlr) 
   logger.setLevel(logging.DEBUG)
   logactive = True
except :
   logactive = False

def logresult( message, error=False ) :
    if logactive :
       if error :
          logger.error( message )
       else :
          logger.info( message ) 
    print message
    
try :
  # as of V33 - adding a settings file to allow easier changes to settings used.  
  # This is just a simple setting.  Allowing testing this free space tool by 
  # adjusting the minimum space available for a test ExoLab so that it can 
  # be set to a much higher number.    
  # This library (ConfigParser) should be in every version of the ExoLab image.. just adding this to a try/except block in 
  # case that assumption is not correct. I 
  import ConfigParser
  options = ConfigParser.ConfigParser(allow_no_value=True)
  options.read( "/home/pi/ExoLab/exolab.conf" )
  SPACETHRESHOLD=int(options.get('options', 'freespacethreshold', 0))
  print "From exolab.conf: SPACETHRESHOLD:", SPACETHRESHOLD,"MB"
except : 
  SPACETHRESHOLD = 300 # how many megabytes we want free 

def command( cmd ) :
    pipe=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    return pipe.communicate()


class diskTools :
      

      PICSFOLDER = "/home/pi/ExoLab/data/pics"

      def __init__(self, picsfolder=PICSFOLDER) :

          self.folder = picsfolder
          self.drivesize = -1
          self.driveused = -1
          self.driveavail = -1
          self.drivepercentused = -1
          self.folderfiles = -1
          self.foldersize = -1
          self.folderaverage = -1.0

          self.getDriveSpace()
          self.getFolderSpace()
          
          
      def getDriveSpace( self ) :
          
          cmd = """df | awk '$NF == "/" { print $2 " " $3 " " $4 " " $5 }'"""
          out,err = command( cmd )
          if len(out)>0 :
             drivedata = out.split()
             self.drivesize = int(drivedata[0])
             self.driveused = int(drivedata[1])
             self.driveavail = int(drivedata[2])
             self.drivepercentused = int(drivedata[3].split('%')[0]) # everything left of the % symbol
          
      
      def getFolderSpace( self ) :
      
          #get size of folder  
          cmd = "du " + self.folder
          out,err = command( cmd )
          if len(out)>0 :
             folderdata = out.split()
             self.foldersize = float(folderdata[0])
          else :
               print "Folder Size Error: ", err
          
          #get number of files
          cmd = "ls -l " + self.folder + " | wc -l"
          out,err = command( cmd )
          if len(out)>0 :
             folderdata = out.split()
             self.folderfiles = int(folderdata[0])
          else :
               print "Folder Files Error: ", err
          
          self.folderaverage = self.foldersize / self.folderfiles
          self.spaceleft = self.driveavail / 1000 - SPACETHRESHOLD
          self.imagesleft = int( self.spaceleft / ( self.folderaverage / 1000)  )


      def rmOldestFiles( self, rmfilecount=0 ) :
      
          os.chdir(self.folder) # Make sure we are in the folder directory, even if started outside
          
          #remove the "x" oldest files
          msg = "Removing " + str(rmfilecount) + " files from " + self.folder
          logresult( msg ) 
          cmd = "ls -tp " + self.folder + " | grep -v '/$' | tail -n "+ str(rmfilecount) +" | tr '\\n' '\\0' | xargs -0 rm --"
          print "rm cmd:", cmd
          command( cmd )
                
      def driveSummary( self ) :
          logresult( "FREESPACE Check: Drive Space Available: " + str( self.driveavail / 1000 ) + "MB / Must be "+ str(SPACETHRESHOLD)+"MB Available" ) 
          print "PICS folder",self.folder
          print "Drive Size",self.drivesize / 1000,"MB"
          print "Drive Used",self.driveused / 1000,"MB"
          print "Drive Avail",self.driveavail / 1000,"MB, There MUST BE",SPACETHRESHOLD,"MB Minimum Avail"
          print "Drive Percent Used",self.drivepercentused
          print "Image Files in Pics Folder",self.folderfiles 
          print "Image Folder Size",self.foldersize / 1000
          print "Image Folder Average File Size", self.folderaverage / 1000.0
          
          print
          print "space until threshold:", self.spaceleft
          print "images until threshold:", self.imagesleft
          print "days left:", self.imagesleft / 16 
          print 
                              
drive = diskTools()
drive.driveSummary()

# drive.imagesleft will be positive if there is still space before we reach the SPACETHRESHOLD
# it will be a negative number if we need to remove older images to stay within the SPACETHRESHOLD value.  
# NOTE: the next step removes files - it's in a loop because the calculation for the number of 
# image files to delete is made based upon the average image size in the folder. 
# So.. we just repeat the delete loop until we really do get to the threshold - so the number of files needed 
# to do that may be different based upon the size of those deleted images.  If they are smaller than the 
# average we'll need to repeat the loop a few times.  
# Out of an abundance of caution I will quit out of the delete loop after 10 iterations - 
# just to give up on this - so it won't continue running if some unexplainable thing 
# is going on.  
cnt = 1 
while drive.imagesleft < 0 :
   print "Loop", cnt
   drive.rmOldestFiles( -drive.imagesleft + 1 )
   # what's the space like now?
   drive = diskTools()
   print "After Cleanup"
   drive.driveSummary()
   cnt+=1
   if cnt > 10 :
      # SHOULD NEVER GET HERE :-)
      logresult( "Aborting delete loop after "+str(cnt-1)+" loops SOMETHING FUNKY GOING ON???", error=True)  
      break
   


  