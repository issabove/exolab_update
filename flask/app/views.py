#V34 10/29/18
from flask import render_template, flash, redirect
from app import app
from .forms import LoginForm
import subprocess
import time
import strandtest
import LCD
import shutil

from wpa_manager import updateNetwork, wifiChoices, pcount

entry_WPA="""
network={{
\tssid=\"{}\"
\tpriority=2
\tpsk=\"{}\"
\tkey_mgmt=WPA-PSK
}}
"""

entry_NONE="""
network={{
\tssid=\"{}\"
\tpriority=2
\tkey_mgmt=NONE
}}
"""

entry_PAEP="""
network={{
\tssid=\"{}\"
\tpriority=2
\tkey_mgmt=WPA-EAP
\tproto=RSN
\tpairwise=CCMP
\tauth_alg=OPEN
\teap=PEAP
\tidentity=\"{}\"
\tpassword=\"{}\"
\tphase1="peaplabel=0"
\tphase2="auth=MSCHAPV2"
}}
"""

# index view function suppressed for brevity
@app.route('/', methods=['GET', 'POST'])
def login():

    if 'wlan0' in open("/etc/dhcpcd.conf").read():
       # We are in hotspot / network setup mode
       devmode = False
    else :
       # Only displayed IF this is manually run from the commandline (not in hotspot/AP mode)  
       if pcount('prod.py')>0 :
          # if pcount > 0 means the prod.py code is running and we are not really in hotspot mode!
          flash( 'DEVELOPMENT MODE') 
          devmode = True
       else :
          # we are likely really still in Hotspot mode - but just in the process of rebooting
          # This was an "unaccounted" for state of the flask server which happens right 
          # when the ExoLab is about to be rebooted after making changes to the wifi
          # config. This may have caused odd wifi problems in the past    
          devmode = False

    #devmode = False 
    form = LoginForm()
           
    # Get list of WiFi networks visible to THIS ExoLab device
    form.ssid.choices=wifiChoices(debug=devmode)
       
    if form.validate_on_submit():
        #print form.restart
        #print form.clear
        #print form.backup
        #print form.ssid
        #print form.security
        if form.restart.data:
            accepted = True
            if (form.security.data == 'WPA-PSK' and len(form.psk.data)>=8 ):
                new_entry=entry_WPA.format(form.ssid.data,form.psk.data,form.security.data)
            elif (form.security.data == 'WPA-PSK' and len(form.psk.data) < 8 ):
                flash('Passkey is not long enough')
                accepted = False
            elif (form.security.data=='NONE'):
                new_entry=entry_NONE.format(form.ssid.data,form.security.data)
            elif (form.security.data=='WPA-PAEP'):
                new_entry=entry_PAEP.format(form.ssid.data,form.username.data,form.password.data)

            if form.ssid.data == 'Select WiFi Network' :
               flash('You must select your WiFi Network from the list.')
               flash('--If you do not see the network listed refresh this page again.')
               flash('--If you still do not see it listed your wifi network is not compatible with the ExoLab.  Contact support for help')
               accepted = False
               
            if(accepted):
                
                #wpa_file = "/home/pi/wpa_supplicant.conf" # DEV - only for testing
                wpa_file = "/etc/wpa_supplicant/wpa_supplicant.conf" # PROD - we will update active wpa_supplicant.conf file
                updateNetwork( form.ssid.data, new_entry, WPA_SUPPLICANT=wpa_file, debug=devmode)
                if 'wlan0' in open("/etc/dhcpcd.conf").read() : 
                   # We are in hotspot/network setup mode... so we must restart
                   flash('Credentials accepted for: ' + form.ssid.data)
                   flash('RESTARTING....')
                   LCD.start()
                   LCD.lcd_string("RESTARTING...",LCD.LCD_LINE_1)
                   LCD.lcd_string("PLEASE WAIT...",LCD.LCD_LINE_2)
                   subprocess.Popen('/home/pi/ExoLab/APclean.sh')
                else :
                   #we are in debug mode experiment mode... so we don't need to reboot or run APclean.sh 
                   #note - this is really just for testng when running run.py manually from command line.  
                   # in this case no need to reboot.  
                   flash('Credentials accepted for: ' + form.ssid.data)
                   flash( wpa_file )
                   LCD.start()
                   LCD.lcd_string("WiFi Updated...",LCD.LCD_LINE_1)
                   LCD.lcd_string(form.ssid.data,LCD.LCD_LINE_2)
                   
            
        elif form.clear.data:
            shutil.copyfile("/home/pi/ExoLab/wpa_backup.conf","/etc/wpa_supplicant/wpa_supplicant.conf")
            LCD.start()
            LCD.lcd_string("CLEARING NTWKS ",LCD.LCD_LINE_1)
            LCD.lcd_string("AND RESTARTING ",LCD.LCD_LINE_2)
            subprocess.Popen('/home/pi/ExoLab/APclean.sh')

        elif form.backup.data:
            LCD.start()
            LCD.lcd_string("RESTORING",LCD.LCD_LINE_1)
            LCD.lcd_string("FACTORY CONFIG",LCD.LCD_LINE_2)
            subprocess.Popen('/home/pi/backup/installer.sh')
            # The installer script will run APclean.sh - 9/18/18 LK
            #subprocess.Popen('/home/pi/ExoLab/APclean.sh')

        
        return redirect('/')
    return render_template('login.html', 
                           title='WPA-2 Credentials',
                           form=form)
