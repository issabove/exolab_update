# V34 10/29/18  
# Adapted from ISS-Above wpa_manager.py - full rights to Magnitude.io for any use.
import shutil
import iw_parse
import subprocess

def updateNetwork( ssid_match, network_block, WPA_COPY="/home/pi/wpa_temp.conf", WPA_SUPPLICANT='/etc/wpa_supplicant/wpa_supplicant.conf', debug=False ) :
    """
      given the target ssid and network block... find the existing network block associated with the ssid given
      and replace it with the new network block.
      
      e.g. 
      
      updataNetwork( "mywifi","network={\n\tssid="mywifi"\n\tpsk="mypassword"\n}")
      
      will read through wpa_supplicant.conf and write all the output in to the WPA_COPY file EXCEPT the network
      block for the given SSID - it will instead replace the network block with the one given.
      then copy the WPA_COPY file over to the WPA_SUPPLICANT file.  
    """
    
    state = 1 
    # 1 = looking for first network={, (just copy each line to the WPA_COPY file) 
    # 2 = network block mode (looking for final "}" for a network block. (save network block)
    # 3 = looking for remaining network={ blocks. 
        
    nw = ""
    ssid = ""
    matched=False
    dnl = False # double (2+) New Lines in sequence in file.  
    nl = False # New Line only in file
    with open(WPA_SUPPLICANT) as wpafp:
       with open( WPA_COPY, "w" ) as wpanew:  
            for cnt, line in enumerate(wpafp):   
                ls = line.replace(" ", "" ) # strip out all spaces to allow matching
                if state == 1 or state == 3 :
                   if "network={" in ls :
                      if debug :
                         print "found network={"
                      state = 2
                      nw = line
                   else :
                       if len(line) <3 :
                          #do we have a new line in the file (oddly wpa_supplicants seem to have CR and NLs)
                          c1 = line[0]
                          if len(line)==2 :
                             c2 = line[1]
                          else :
                             c2 = "x"
                          
                          if ord(c1) == 13 or ord(c1) == 10 or ord(c2) == 13 or ord(c2) == 10 :
                          
                             if debug:
                                print "LEN", len(line)
                                print "ASCII value: ", ', '.join(str(ord(c)) for c in line)
                                
                             if nl :
                                print "DOUBLE NEW LINES" 
                                dnl = True
                             else :
                                dnl = False
                             
                             nl = True
                             if debug and not dnl:
                                print "BLANK LINE ONLY"
                                
                          else :
                             nl = False
                             dnl = False
                       
                       if not dnl :
                          if debug:
                             print("Line {}: {}".format(cnt, line))   
                          wpanew.write( line )
                          
                elif state == 2 :
                     # In network block mode - just copy everything to nw variable until we find the final "}" 
                     nw=nw+line
                     nl=False
                     dnl=False 
                     if "}" in line :
                        # found the close to the current network={ block
                        state = 3
                        if ssid == ssid_match :
                           if debug :
                              print "Matched to", ssid_match, "WE WILL REPLACE" 
                           # we will write the new network_block instead
                           if ord(network_block[0]) == 10 or ord(network_block[0]) == 13 :
                              wpanew.write(network_block[1:])
                           else :
                              wpanew.write(network_block)
                           
                           if debug:
                              print network_block
                           matched = True
                        else :
                            # just copy the network block to the new file
                            if debug :
                               print "COPY NETWORK:"
                               print nw
                               
                            wpanew.write( nw )
                            nw=""
                            ssid=""
                     elif "ssid=" in ls :
                          ssid = line.split('"')[1]
                          if debug :
                             print "ssid=",ssid 
            
            if not matched :
               # so we've copied all the network={...} blocks from the existing wpa_supplicant.conf file 
               # and the given ssid has NOT been found in the existing wpa_supplicant file   
               # so now we'll just add the given network block to the end.
               if debug :  
                  print "ADDING ", ssid_match
                  print network_block
                                
               if ord(network_block[0]) != 10 and ord(network_block[0]) != 13 :
                  if debug: 
                     print "NEW LINE Not In Source Network provided"
                     
                  wpanew.write("\n")
                  
               wpanew.write(network_block)
    
    # move temp wpa to replace the active wpa file
    if debug: 
       print "Moving",WPA_COPY,"TO",WPA_SUPPLICANT
                  
    shutil.move(WPA_COPY, WPA_SUPPLICANT)

def wifiChoices(debug=False) :
    """
    using the iw_parse library (modified from version on github) get
    the list of networks visible to this raspberry pi and construct
    a list containing them.  
    """
    iwscanresults = iw_parse.get_interfaces(interface='wlan0')
    if debug:
       print "iwscanresults"
       print iwscanresults
       print "# of WiFi AP's found", len(iwscanresults)
    wifi_uniq = []
    for nw in iwscanresults :
        nw_ssid=nw['Name']
        if len(nw_ssid) > 0 :
           if debug : 
              print 'ssid:',nw_ssid,nw['Encryption'],
           if nw_ssid not in wifi_uniq :
              wifi_uniq.append(nw_ssid)
              if debug : 
                 print
           else :
              if debug :
                 print ' duplicate'
          
    wifi_uniq.sort(key=lambda v: v.upper())
    if debug :
       print "Uniq SSID's", len(wifi_uniq)
       print wifi_uniq       
    
    ssid_choices = []
    ssid_choices.append(('Select WiFi Network','-- Select WiFi Network --')) 
    for nw in wifi_uniq :
        ssid_choices.append( (nw,nw) )
    
    return ssid_choices

def pcount( pname = 'prod.py'  ):
    
    try:
       cmd = "ps -aef | grep -i '"+ pname + "' | grep -v 'grep' | awk '{ print $2 }'" 
       pipe=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
       out,err=pipe.communicate()
       result=out.decode()
       pc = len(result.split()) 
    except Exception, err: 
       print "WARNING pcount:", repr(err)
       pc=0
        
    return pc
    